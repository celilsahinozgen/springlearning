package tr.com.celil.springlearning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tr.com.celil.springlearning.entity.Personel;


import java.util.List;

@Repository
public interface PersonelRepository extends JpaRepository<Personel,Long> {

  //  List<Personel> findPersonelById(Long id);

    // Query JPQL
    // Query Native
//    @Query(nativeQuery = true, value = "select * from tblpersonel")
  //  @Query("select p from Personel p")
   // List<Personel> tumpersonellerigetir();



}

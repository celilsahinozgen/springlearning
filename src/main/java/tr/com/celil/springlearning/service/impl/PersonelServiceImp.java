package tr.com.celil.springlearning.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tr.com.celil.springlearning.entity.Personel;
import tr.com.celil.springlearning.repository.PersonelRepository;
import tr.com.celil.springlearning.service.PersonelService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PersonelServiceImp implements PersonelService {

    private final PersonelRepository personelRepository;


    @Override
    public Personel save(Personel personel) {
        return personelRepository.save(personel);
    }

    @Override
    public void update(Personel personel) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public Optional<Personel> findById(Long id) {

        Optional<Personel> optionalPersonel = personelRepository.findById(id);

        if (optionalPersonel.isEmpty()){
            throw new RuntimeException("Personel bulunamadı");
        }

         return optionalPersonel;
    }

    @Override
    public List<Personel> findAll() {
        return personelRepository.findAll();
    }
}

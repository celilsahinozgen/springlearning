package tr.com.celil.springlearning.service;

import org.springframework.stereotype.Repository;
import tr.com.celil.springlearning.entity.Personel;

import java.util.List;
import java.util.Optional;

public interface PersonelService {

    Personel save(Personel personel);
    void update(Personel personel);
    void delete(Long id);
    Optional<Personel> findById(Long id);
    List<Personel> findAll();





    }

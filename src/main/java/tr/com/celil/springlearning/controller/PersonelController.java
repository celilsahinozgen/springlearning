package tr.com.celil.springlearning.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tr.com.celil.springlearning.entity.Personel;
import tr.com.celil.springlearning.service.PersonelService;

import java.util.List;
import java.util.Optional;

@RequestMapping("/api")
@RestController
@RequiredArgsConstructor
public class PersonelController {

    private final PersonelService personelService;

    @GetMapping("/tumumpersonel")
    public ResponseEntity<List<Personel>> findAll(){
        return ResponseEntity.ok().body(personelService.findAll());
    }

//    @DeleteMapping

    @GetMapping("/tekpersonelgetir/{id}")
    public ResponseEntity<Personel> findById(@PathVariable Long id){

        Optional<Personel> optionalPersonel;

        try {
            optionalPersonel = personelService.findById(id);
        } catch (RuntimeException e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(optionalPersonel.get());
    }


   @PostMapping("/save")
   public ResponseEntity<Personel> save(Personel personel) {

        if (personel.getId() != null) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(personelService.save(personel));
    }
    @PutMapping("/update")
    public ResponseEntity<Void> update(Personel personel) {

        if (personel.getId() == null) {
            ResponseEntity.badRequest().build();
        }

        personelService.save(personel);
        return ResponseEntity.ok().build();
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            personelService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }




}
